module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: {
        'bg-camera': "url('/public/img/1.jpg')",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],

}
