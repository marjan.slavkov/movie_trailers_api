<?php include ('partials/header.html') ?>

<div
    x-data="contactForm()"
    class="flex flex-col h-full">

    <div class="w-full h-screen bg-gray-800 flex flex-none justify-center items-center bg-camera">
        <div class="relative w-4/12 flex flex-col items-center">
            <div class="w-full flex items-center justify-between">
                <form @submit.prevent class="p-4 bg-gray-600 rounded space-y-3">
                    <input
                        x-model="formData.name"
                        type="text"
                        class="w-full p-2 rounded-md outline-none border-gray-300
                        shadow-sm focus:border-green-500 focus:ring focus:ring-green-300 focus:ring-opacity-10"
                        placeholder="Enter your name">
                    <input
                        x-model="formData.email"
                        type="email"
                        class="w-full p-2 rounded-md outline-none border-gray-300
                        shadow-sm focus:border-green-500 focus:ring focus:ring-green-300 focus:ring-opacity-10"
                        placeholder="Enter your email">
                    <textarea
                        x-model="formData.msg"
                        class="w-full p-2 rounded-md outline-none border-gray-300
                        shadow-sm focus:border-green-500 focus:ring focus:ring-green-300 focus:ring-opacity-10"
                        placeholder="Enter a message"
                        rows="5">
                    </textarea>
                    <button
                        type="submit"
                        @click="postContactForm()"
                        class="rounded bg-blue-500 text-white p-2">Submit</button>
                </form>
            </div>
        </div>
    </div>

<?php include ('partials/footer.html') ?>
