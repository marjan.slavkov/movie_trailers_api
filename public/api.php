<?php

use App\Services\Api;
use App\Services\MovieTrailer;

require "../vendor/autoload.php";

header('Access-control-allow-origin: *');
header('Content-Type: application/json');


if(request('opt') == 1){
  $omdb = Api::load('omdb');
  echo $omdb->getTitlesResults($_GET['query'], $_GET['page'] ?? 1);
}


if(request('opt') == 2){
    $trailers = new MovieTrailer($_GET['query']);
  echo $trailers->getTrailers();
}




