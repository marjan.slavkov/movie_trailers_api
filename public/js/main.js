function getSearchResults() {
    return {
        query: {
            debounced: '',
            current: '',
            last: '',
        },
        searchedMovie: '',
        results: [],
        movieTitles: [],
        selectedTitle: {
            info: [],
            trailers: [],
        },
        pages: {
            total: '',
            currentPage: 1,
            nextPages: [],
            lastPages: [],
            pageSeparator: true,
        },
        open: {
            dropdown: false,
            cards: false,
            modal: false,
        },
        searchBtnStatus: "Search",
        error: {
            hasError: false,
            msg: null
        },
        focusedOptionIndex: null,


        async searchboxTitles() {

            this.searchBtnStatus = "Loading..."
            try {
                var res = await axios.get('api.php', {
                    params: {
                        opt: 1,
                        query: this.query.debounced
                    }
                })
            } catch (e) {
                console.log('GOT ERROR', e)
            }
            this.searchBtnStatus = "Search"
            if (res.data.error) {
                this.error.msg = res.data.error;
                this.error.hasError = true;
                return;
            }
            this.results = this.sortMoviesByName(res.data.data);
            this.open.dropdown = true

        },

        async browseTitles(option = 1, pageNum = 1) {

            try {
                var res = await axios.get('api.php', {
                    params: {
                        opt: option,
                        page: pageNum,
                        query: this.searchedMovie
                    }
                })
            } catch (e) {
                console.log('GOT ERROR', e)
            }
            if (res.data.error) {
                this.error.msg = res.data.error;
                this.error.hasError = true;
                return;
            }

            if (option === 1){
                this.pages.total = res.data.totalPages;
                this.movieTitles = this.sortMoviesByName(res.data.data);
                this.generatePagination();
            }

            if (option === 2){
                this.selectedTitle.info = res.data.info;
                this.selectedTitle.trailers = res.data.trailers;
            }
        },

        init: function () {

            //watch for the change of query variable and search for a movie on change
            this.$watch('query.debounced', (value) => {
                if (!this.query.debounced) {
                    this.closeAll();
                    return;
                }

                this.searchboxTitles();
            })

            this.$watch('pages.currentPage', (value) => {
                this.generatePagination();
            })
        },

        selectMovie: function () {
            let movieId = this.results[this.focusedOptionIndex].movieId
            this.open.dropdown = false;
            this.results = [];
            this.displayTrailers(movieId);
        },

        sortMoviesByName: function (titles) {
            if (titles) {
                this.results = this.results.sort(function (a, b) {
                    let nameA = a.movieTitle.toUpperCase(); // ignore upper and lowercase
                    let nameB = b.movieTitle.toUpperCase(); // ignore upper and lowercase
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    return 0;
                });
                return titles;
            }
        },

        sortMoviesByYear: function () {
            this.results = this.results.sort((val1, val2) => val1.year - val2.year);
        },

        //removing all components if the input field is empty or user clicks away from the searchbar
        closeAll: function () {
            this.open.dropdown = false;
            this.error.hasError = false;

        },

        closeModal: function (){
          this.open.modal = false;
          this.selectedTitle.info = [];
          this.selectedTitle.trailers = [];
        },

        displayTitles: function () {
            this.query.debounced = ''
            if (!this.query.current) {
                this.open.dropdown = false;
                return;
            }
            this.searchedMovie = this.query.current;
            this.browseTitles();
            this.query.last = this.query.current;
            this.query.current = '';
            this.open.cards = true;
            this.open.dropdown = false;
            this.results = [];
            this.pages.currentPage = 1;
        },

        generatePagination: function (){
            if (this.pages.total === 1 || this.pages.currentPage + 4 > this.pages.total){
                return;
            }
            this.pages.pageSeparator = true;
            if(this.pages.total - this.pages.currentPage >= 6){
                this.pages.nextPages = this.pages.currentPage > 1
                    ? Array.from({length: 3}, (v, i) => (this.pages.currentPage - 1) + i)
                : [1,2,3];
                this.pages.lastPages = Array.from({length: 3}, (v, i) => (i - 2) + this.pages.total);
            } else {
                this.pages.nextPages = Array.from({length: 5}, (v, i) => this.pages.currentPage + i);
                this.pages.lastPages = [];
                this.pages.pageSeparator = false;
            }
        },


        toNextPage: function (){
            if (this.pages.currentPage + 1 >= this.pages.total){
                return;
            }
            this.displaySelectedPage(this.pages.currentPage + 1);
        },

        toPreviousPage: function (){
            if (this.pages.currentPage - 1 < 1){
                return;
            }
            this.displaySelectedPage(this.pages.currentPage - 1);
        },

        displaySelectedPage(page){
            this.browseTitles(1, page);
            this.pages.currentPage = page;

        },

        displayTrailers: function (movieId) {

            this.open.modal = true;
            this.searchedMovie = movieId;
            this.browseTitles(2)
            this.searchedMovie = this.query.last;
        },
    }
}

function contactForm(){
    return {
        formData: {
            name: '',
            email: '',
            msg: '',
        },

        postContactForm(){
            axios.post('post.php', {
                name: this.formData.name,
                email: this.formData.email,
                msg: this.formData.msg,
            }).then(response => console.log(response))
        },
    }
}
