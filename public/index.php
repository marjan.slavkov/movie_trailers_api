<?php include ('partials/header.html') ?>

<div
    x-data="getSearchResults()"
    x-init="init()"
    class="flex flex-col h-full">

    <div class="w-full h-3/6 bg-gray-800 flex flex-none justify-center items-center bg-camera">
        <div class="relative w-4/12 flex flex-col items-center">
            <div class="w-full flex items-center justify-between">
                <input
                    x-model.debounce.600="query.debounced"
                    x-model="query.current"
                    @keyup.enter.stop.prevent="query.debounced"
                    @keydown.enter="displayTitles()"
                    @click.away="open.dropdown = false"
                    type="text"
                    class="w-full p-2 rounded-md outline-none border-gray-300
                    shadow-sm focus:border-green-500 focus:ring focus:ring-green-300 focus:ring-opacity-10"
                    placeholder="Enter a movie title">
                <button x-text="searchBtnStatus".
                        @click="displayTitles()"
                        class="absolute w-28 rounded-r p-2 right-0 bg-gray-200"
                >Search
                </button>

            </div>
            <div
                x-show="open.dropdown"
                class="absolute rounded top-10 w-full bg-white border-indigo-300">
                <template x-for="(movie, index) in results" :key="index">
                    <div
                        @mouseenter="focusedOptionIndex = index"
                        @mouseleave="focusedOptionIndex = null"
                        @click="selectMovie(); displayTitles()"
                        :class="{ 'text-white bg-indigo-600': index === focusedOptionIndex, 'text-gray-900': index !== focusedOptionIndex }"
                        class="px-2 py-1 rounded flex justify-between cursor-default">
                        <span x-text="movie.movieTitle"></span>
                        <span x-text="movie.year"></span>
                    </div>
                </template>
            </div>
            <div class="h-12">
                <p
                    x-show="error.hasError"
                    x-text="error.msg"
                    class="p-2 text-white text-sm"
                ></p>
            </div>
        </div>

    </div>

       <!-- cards -->
    <div class="w-full bg-gray-600 flex flex-col flex-1 items-center">
        <div x-show="open.cards"
             class="w-10/12 flex flex-col items-center space-y-10 rounded py-6 mx-auto">
            <div class="flex flex-wrap justify-center space-x-6 space-y-6">
                <template x-for="(title, index) in movieTitles" :key="index">
                    <div class="w-56 space-y-3 rounded bg-gray-300">
                        <div class="w-full overflow-hidden">
                            <img class="w-full h-56 rounded-t object-cover object-center" :src="title.poster"
                                 alt="avatar">
                        </div>
                        <div class="py-4 px-6">
                            <div class="h-18">
                                <h1 x-text="title.movieTitle"
                                    class="text-md font-semibold text-gray-800"></h1>
                            </div>
                            <div class="flex items-center mt-4 text-gray-700">
                                <h1>Release year:</h1>
                                <h1 x-text="title.year"
                                    class="px-2 text-sm"></h1>
                            </div>
                            <div class="flex items-center mt-4 text-gray-700">
                                <svg class="h-6 w-6 fill-current" viewBox="0 0 20 20">
                                    <path
                                        d="M16.469,8.924l-2.414,2.413c-0.156,0.156-0.408,0.156-0.564,0c-0.156-0.155-0.156-0.408,0-0.563l2.414-2.414c1.175-1.175,1.175-3.087,0-4.262c-0.57-0.569-1.326-0.883-2.132-0.883s-1.562,0.313-2.132,0.883L9.227,6.511c-1.175,1.175-1.175,3.087,0,4.263c0.288,0.288,0.624,0.511,0.997,0.662c0.204,0.083,0.303,0.315,0.22,0.52c-0.171,0.422-0.643,0.17-0.52,0.22c-0.473-0.191-0.898-0.474-1.262-0.838c-1.487-1.485-1.487-3.904,0-5.391l2.414-2.413c0.72-0.72,1.678-1.117,2.696-1.117s1.976,0.396,2.696,1.117C17.955,5.02,17.955,7.438,16.469,8.924 M10.076,7.825c-0.205-0.083-0.437,0.016-0.52,0.22c-0.083,0.205,0.016,0.437,0.22,0.52c0.374,0.151,0.709,0.374,0.997,0.662c1.176,1.176,1.176,3.088,0,4.263l-2.414,2.413c-0.569,0.569-1.326,0.883-2.131,0.883s-1.562-0.313-2.132-0.883c-1.175-1.175-1.175-3.087,0-4.262L6.51,9.227c0.156-0.155,0.156-0.408,0-0.564c-0.156-0.156-0.408-0.156-0.564,0l-2.414,2.414c-1.487,1.485-1.487,3.904,0,5.391c0.72,0.72,1.678,1.116,2.696,1.116s1.976-0.396,2.696-1.116l2.414-2.413c1.487-1.486,1.487-3.905,0-5.392C10.974,8.298,10.55,8.017,10.076,7.825"></path>
                                </svg>
                                <h1
                                    @click="displayTrailers(title.movieId)"
                                    class="px-2 text-sm cursor-pointer">Trailers</h1>
                            </div>
                        </div>
                    </div>
                </template>
            </div>

            <!--pagination-->
            <div x-show="pages.total > 1"
                 style="display: none"
                class="text-black bg-gray-600 text-gray-100 px-4 py-3 flex items-center justify-between sm:px-6">
                <div class="flex-1 flex justify-between sm:hidden">
                    <a
                        @click="toPreviousPage()"
                        class="relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:text-gray-500">
                        Previous
                    </a>
                    <button
                        @click="toNextPage()"
                        class="ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:text-gray-500">
                        Next
                    </button>
                </div>
                <div class="sm:flex-1 sm:flex sm:items-center sm:justify-between">
                    <div>
                        <nav class="relative z-0 inline-flex shadow-sm -space-x-px" aria-label="Pagination">
                            <button
                                @click="toPreviousPage()"
                                class="relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
                                <span class="sr-only">Previous</span>
                                <!-- Heroicon name: chevron-left -->
                                <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                     fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd"
                                          d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                                          clip-rule="evenodd"/>
                                </svg>
                            </button>
                            <template x-for="(page, index) in pages.nextPages" :key="index">
                                <button
                                    x-text="page"
                                    @click.="displaySelectedPage(page)"
                                    :class="{'bg-gray-200' : page === pages.currentPage}"
                                    class="relative focus:outline-none inline-flex items-center px-4 py-2 border
                                   border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-200">

                                </button>
                            </template>
                            <span x-show="pages.pageSeparator"
                                  style="display: none"
                                  class="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700">
                          ...
                    </span>
                            <template x-for="(page, index) in pages.lastPages" :key="index">
                                <button
                                    x-text="page"
                                    @click="displaySelectedPage(page)"
                                    :class="{'bg-gray-200' : page === pages.currentPage}"
                                    class="relative focus:outline-none inline-flex items-center px-4 py-2 border
                                   border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-200">
                                </button>
                            </template>
                            <button @click="toNextPage()"
                                    class="relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
                                <span class="sr-only">Next</span>
                                <!-- Heroicon name: chevron-right -->
                                <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                     fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd"
                                          d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                          clip-rule="evenodd"/>
                                </svg>
                            </button>
                        </nav>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- modal div -->

        <div
            x-on:close.stop="closeModal()"
            x-on:keydown.escape.window="closeModal()"
            x-show="open.modal"
            class="fixed top-0 inset-x-0 px-4 pt-6 z-50 sm:px-0 sm:flex sm:items-top sm:justify-center"
            style="display: none;"
        >
            <div x-show="open.modal"
                 class="fixed inset-0 transform transition-all"
                 @click="closeModal()"
                 x-transition:enter="ease-out duration-300"
                 x-transition:enter-start="opacity-0"
                 x-transition:enter-end="opacity-100"
                 x-transition:leave="ease-in duration-200"
                 x-transition:leave-start="opacity-100"
                 x-transition:leave-end="opacity-0"
            >
                <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
            </div>

            <div x-show="open.modal" class="bg-white rounded-lg overflow-hidden shadow-xl transform transition-all sm:w-4/6"
                 x-transition:enter="ease-out duration-300"
                 x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                 x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
                 x-transition:leave="ease-in duration-200"
                 x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
                 x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95">
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4 space-y-5">

                    <!--movie info card-->
                    <div class="border border-gray-100 p-2 rounded sm:flex sm:items-start">
                        <div class="mt-3 text-center sm:mt-0 sm:text-left">
                            <div class="w-full flex space-x-3">
                                <div class="w-32 overflow-hidden">
                                    <img :src="selectedTitle.info.poster" alt="">
                                </div>
                                <div class="flex flex-col space-y-3">
                                    <h1 x-text="selectedTitle.info.movieTitle" class="text-xl"></h1>
                                    <span class="flex flex-col text-sm">Starring:
                                        <h1 x-text="selectedTitle.info.actors" class="text-sm"></h1>
                                    </span>

                                    <span class="flex flex-col text-sm">Released on:
                                        <h1 x-text="selectedTitle.info.releaseDate"></h1>
                                    </span>
                                    <span class="flex flex-col text-sm">Runtime:
                                        <h1 x-text="selectedTitle.info.runtime"></h1>
                                    </span>
                                </div>
                            </div>

                            <div class="mt-2">
                                <a
                                    :href="selectedTitle.info.movieId"
                                    class="p-1 mt-2 rounded text-black bg-yellow-400 font-bold"
                                    target="_blank"
                                >Imbd</a>
                            </div>
                        </div>
                    </div>

                    <!--trailers-->

                   <div class="flex flex-wrap w-full text-xs items-center">
                       <template x-for="(trailer, index) in selectedTitle.trailers" :key="index">
                           <div class="p-1 flex flex-col w-40 h-40 items-center rounded space-x-3 space-y-3">
                               <div class="w-32 overflow-hidden">
                                   <img :src="trailer.urlThumbnail" alt="">
                               </div>
                               <a :href="trailer.urlVideo"
                                  x-text="trailer.trailerTitle"
                                  target="_blank"
                               ></a>
                           </div>
                       </template>
                   </div>
                </div>

                <div class="px-6 py-4 bg-gray-100 text-right space-x-4">
                    <button @click="closeModal()">
                        Close
                    </button>
                </div>

            </div>
        </div>

        <!--end-->

    </div>
<!--<div class="bg-gray-800 flex justify-center items-center">
    <h3 class="text-white">Cannot find a movie trailer? <a class="underline" href="contact-form.php">Contact us</a></h3>
</div>-->
</div>

<?php include ('partials/footer.html') ?>
