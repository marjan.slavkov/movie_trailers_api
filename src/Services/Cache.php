<?php


namespace App\Services;


class Cache
{
    protected static function filepath($filename){
        return CACHE_FOLDER . $filename;
    }

    public static function get($key){
        $filename = md5($key);

        return file_exists(self::filepath($filename)) ? file_get_contents(self::filepath($filename)) : null;
    }

    public static function put($key, $value){
        $filename = md5($key);

        file_put_contents(self::filepath($filename), $value);
    }
}