<?php

namespace App\Services;

class YoutubeApi extends ApiService
{
    public function setSearchUrl($query, $option=null, $page=null)
    {
        $this->query = $query;
        $encodedQuery = http_build_query([
            "part" => "snippet",
            "maxResults" => 10,
            "q" => $this->query . "+trailer",
            "topicId" => "movies",
            "key" => $this->apiProvider['key']
        ]);
        $this->searchUrl = "{$this->apiProvider['baseUrl']}$encodedQuery";
        return $this;
    }


    public function getSearchResults()
    {
        if ($cachedContent = Cache::get($this->query)){
            return json_decode($cachedContent, true);
        }

        $trailers = json_decode($this->getResults(), true);

        $youtubeTitles = array_values($trailers['items']);

        $arr = [];
        foreach ($youtubeTitles as $key) {
            if (!isset($key['id']['videoId']))
                continue;
            array_push($arr, [
                'trailerTitle' => $key['snippet']['title'],
                'urlVideo' => "https://www.youtube.com/watch?v={$key['id']['videoId']}",
                'urlThumbnail' => $key['snippet']['thumbnails']['default']['url']
            ]);
        }

        Cache::put($this->query, json_encode($arr));
        return $arr;
    }

}
