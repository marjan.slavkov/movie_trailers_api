<?php

namespace App\Services;

class RapidApi extends ApiService
{

  public function setSearchUrl($query, $option=null, $page=null)
  {
    $query = urlencode($query);
    $this->searchUrl = "{$this->apiProvider['baseUrl']}title/auto-complete?q=$query&rapidapi-key={$this->apiProvider['key']}";
  }

}

