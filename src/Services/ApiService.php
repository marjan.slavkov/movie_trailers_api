<?php

namespace App\Services;

abstract class ApiService
{
    public $providerName;
    protected $apiProvider;
    protected $searchUrl;
    protected $query;

    public function __construct($provider)
    {
        $this->apiProvider = PROVIDERS[$provider];
        $this->providerName = $provider;
    }

    public function getSearchUrl()
    {
        return $this->searchUrl;
    }

    protected function getResults()
    {
        $apiConnection = new ApiConnection($this);
        return $apiConnection->curlConnection();
    }

    abstract function setSearchUrl($query, $option, $page);

}

