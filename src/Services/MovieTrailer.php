<?php

namespace App\Services;

class MovieTrailer
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function getTrailers()
    {
        $omdb = Api::load('omdb');;
        $movieInfo = $omdb->getMovieInfo($this->query);
        $youtube = Api::load('youtube');;
        $trailers = $youtube->setSearchUrl($movieInfo['movieTitle'])->getSearchResults();

        $arr = [
            'info' => $movieInfo,
            'trailers' => $trailers
        ];

        return json_encode($arr);
    }
}
