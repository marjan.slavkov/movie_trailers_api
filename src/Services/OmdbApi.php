<?php

namespace App\Services;

class OmdbApi extends ApiService
{

    public function setSearchUrl($query, $option = "s", $page=1)
    {
        $this->query = $query;
        $encodedQuery = http_build_query([
            $option => $query,
            "apikey" => $this->apiProvider['key'],
            "page" => $page
        ]);
        $this->searchUrl = "{$this->apiProvider['baseUrl']}?$encodedQuery";

        return $this;
    }

    public function getTitlesResults($query, $page=1)
    {
        $this->setSearchUrl($query, "s", $page);
        $omdbTitles = json_decode($this->getResults(), true);

        if (isset($omdbTitles['Error'])) {
            echo json_encode(['error' => $omdbTitles['Error']]);
            die();
        }

        $arr = ['totalPages' => ceil($omdbTitles['totalResults']/10)];
        $omdbTitles = array_values($omdbTitles['Search']);


        foreach ($omdbTitles as $title) {
            $arr['data'][] = ['movieId' => $title['imdbID'], 'movieTitle' => $title['Title'], 'year' => $title['Year'], 'poster' => $title['Poster']];
        }
        return json_encode($arr);
    }


    public function getMovieInfo($imdbId){

        if ($cachedContents = Cache::get($imdbId)){
            return json_decode($cachedContents,true);
        }

        $this->setSearchUrl($imdbId, 'i');

        $movieInfo = json_decode($this->getResults(), true);

        if (isset($movieInfo['Error'])) {
            echo json_encode([$movieInfo['Error']]);
            die();
        }

        $cachedContents = [
            'movieId' => "https://www.imdb.com/title/{$movieInfo['imdbID']}",
            'movieTitle' => $movieInfo['Title'],
            'year' => $movieInfo['Year'],
            'actors' => $movieInfo['Actors'],
            'poster' =>$movieInfo['Poster'],
            'releaseDate' =>$movieInfo['Released'],
            'runtime' =>$movieInfo['Runtime'],
        ];

        Cache::put($imdbId, json_encode($cachedContents));
        return $cachedContents;
    }
}

