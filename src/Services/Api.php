<?php


namespace App\Services;


class Api
{
    public static function load($provider)
    {
        if (array_key_exists($provider, PROVIDERS)) {

            $providerName = ucfirst($provider);
            $className = "\App\Services\\{$providerName}Api";
            return new $className($provider);

        } else {
            echo json_encode(['error' => 'No provider defined in config file']);
            die();
        }
    }
}