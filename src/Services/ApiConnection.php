<?php

namespace App\Services;

class ApiConnection
{
  private $api;

  public function __construct(ApiService $api){
    $this->api = $api;
  }

  public function curlConnection()
  {
    $apiOptions = PROVIDERS[$this->api->providerName]['curlOptions'];
    $apiOptions[CURLOPT_URL] = $this->api->getSearchUrl();
    $curlObj = curl_init();
    curl_setopt_array($curlObj, $apiOptions);
    $returnData = curl_exec($curlObj);
    if (curl_errno($curlObj)) {
      //error message
      $returnData = curl_error($curlObj);
    }
    curl_close($curlObj);
    return $returnData;
  }
}
