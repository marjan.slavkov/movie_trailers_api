<?php

function request($input, $default = 1) {
    if (!isset($_GET[$input]) || !$_GET[$input]) {
        return $default;
    }
    return $_GET[$input];
}


