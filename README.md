#Simple website and web API for movie trailers search


##Technologies

- PHP
- Alpine.js
- tailwindcss


##Installation

Run:
1. composer install
2. composer dump


##Launch

Run "php -S localhost:8000" in the "public" folder.
