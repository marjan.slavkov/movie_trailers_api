<?php

const PROVIDERS = [

  'youtube' => [
    'baseUrl' => 'https://youtube.googleapis.com/youtube/v3/search?',
    'key' => 'AIzaSyCaxGpSn69DuLe8g5SVOhfYTDuuUB5lvCY',
    'curlOptions' => [
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => [
        'Accept: application/json'
      ],
    ],
  ],

  'rapidapi' => [
    'baseUrl' => 'https://imdb8.p.rapidapi.com/',
    'key' => '33641e7847mshb1c5dedf47d812cp164aacjsn58b70561a3e8',
    'curlOptions' => [
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => [
        "x-rapidapi-host: imdb8.p.rapidapi.com",
        "x-rapidapi-key: 33641e7847mshb1c5dedf47d812cp164aacjsn58b70561a3e8",
      ]
    ],
  ],

  'omdb' => [
    'baseUrl' => 'http://www.omdbapi.com/',
    'key' => 'c116aec1',
    'curlOptions' => [
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => [
        'Accept: application/json',
      ]
    ],
    ],
];

const CACHE_FOLDER = 'cache/';

